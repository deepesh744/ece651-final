import React from 'react'
import { createContext, useContext, useEffect, useState } from 'react'
import axios from "axios";
import { auth, db } from "./firebase/firebase";
import { onAuthStateChanged } from "firebase/auth";
import { doc, onSnapshot } from 'firebase/firestore';

const CryptoContext = createContext();

const ContextAPI = ({children}) => {

    // Create state for balance, coins and stocks
    const [coins,setCoins] = useState([]);
    const [loading,setLoading] = useState(false);

    const [user,setUser] = useState(null);

    const [alert,setAlert] = useState({
        open: false,
        message: '',
        type: 'success',
    });

    //Stock Playground
    const [selectedStock, setSelectedStock] = useState("AAPL");
    const [myStockList, setMyStockList] = useState({});
    const [balance, setBalance] = useState("");
    const [watchlist,setWatchList] = useState([]);

    useEffect(() => {
        if(user) {
            const databaseRef = doc(db, "watchlist", user.uid);

            var unsubscribe = onSnapshot(databaseRef,(coin) => {
                if(coin.exists()){
                    console.log("Stocks in watchlist are: ", coin.data().coins);
                    setWatchList(coin.data().coins);
                } else {
                    console.log("No Stock present in Watchlist")
                }
            });
            return () => {
                unsubscribe();
            }
        }
    },[user])



    useEffect(() => {
        onAuthStateChanged(auth , user => {
            if(user) setUser(user);
            else setUser(null);
        });
    },[]);

    const fetchCoins = async() => {
        setLoading(true);
        const {data} = await axios.get(`https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false`);
        setCoins(data);
        setLoading(false);
    }
    

  return (
    //   Wrap the whole app in CryptoApp.Provider: coins and loading 
    <CryptoContext.Provider value={{coins,loading, fetchCoins, user, alert, setAlert, watchlist, selectedStock,
        setSelectedStock,
        myStockList,
        setMyStockList,
        balance,
        setBalance,}}>
        {children}
    </CryptoContext.Provider>
  )
}

export default ContextAPI

// To export these states to whole of our app we are gonna make use of the hook called “useContext” Hook

export const CryptoState = () => {
    return useContext(CryptoContext);
}