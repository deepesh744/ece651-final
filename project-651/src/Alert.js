import { Snackbar } from '@mui/material';
import React from 'react'
import MuiAlert from '@material-ui/lab/Alert'
import {CryptoState} from "./ContextAPI"

const Alert = () => {

    const { alert,setAlert } = CryptoState();

    const handleClose = (event, reason) => {
        if (reason === "clickaway") {
          return;
        }
        setAlert({ open: false });
      };

  return (
    <Snackbar
    open={alert.open}
    autoHideDuration={2000}
    onClose={handleClose}
    >
        <MuiAlert 
        elevation={10}
        variant="filled"
        onClose={handleClose}
        severity={alert.type}
        >
            {alert.message}
        </MuiAlert>
    </Snackbar>
  )
}

export default Alert