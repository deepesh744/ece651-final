import React from 'react'
import {AppBar,Container,Toolbar,Typography,Select,MenuItem,makeStyles,createTheme,ThemeProvider} from '@material-ui/core';
import { useHistory } from "react-router-dom"
import {CryptoState} from "../../ContextAPI"
import AuthPopup from '../Authentication/AuthPopup'
import UserProfileBar from './UserProfileBar'

const useStyles = makeStyles(() => ({
    title: {
        flex: 1,
        color: "gold",
        fontFamily: "Montserrat",
        fontWeight:"bold",
        cursor: "pointer",
    },
    gotoWatchlist:{
        cursor: "pointer",
        fontWeight:"bold",
        fontFamily: "Montserrat",
        marginLeft: 10,
    }
}));

function Header() {

const classes = useStyles();
const {user} = CryptoState();

const darkTheme = createTheme({
    palette: {
        primary:{
            main: "#fff",
        },
        type: "dark",
    },
});

const history = useHistory();
//If user is present then Display "User Profile page" else keep "Login" button as it is

  return (
      <ThemeProvider theme={darkTheme}> 
        <AppBar color='transparent' position='static'>
            <Container>
                <Toolbar>
                    <Typography onClick={() => history.push("/")} className={classes.title} variant='h5'>
                        INVESPAL
                    </Typography>
                    {user? <UserProfileBar /> :<AuthPopup />}
                    <Typography onClick={() => {history.push("/watchlist")}} className={classes.gotoWatchlist} variant="subtitle1">
                        Watchlist
                    </Typography>
                </Toolbar>
            </Container> 
        </AppBar>
    </ThemeProvider>
  )
}

export default Header