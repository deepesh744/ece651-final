import React, { useEffect, useState } from "react";
// import { useAuthState } from "react-firebase-hooks/auth";
// import { onAuthStateChanged } from "firebase/auth";
import { useHistory } from "react-router-dom";
import { TwitterTweetEmbed } from "react-twitter-embed";
import { auth } from "../../firebase/firebase";
import { TextField } from "@mui/material";
import config from "../../config/default";
import "./Tweets.css";

function Tweets() {
//   const [user, loading, error] = useAuthState(auth);
  const [tweetId, setTweetId] = useState([]);
  const navigate = useHistory();
  const [load, setLoad] = useState(true);
  const [search, setSearch] = useState("btc");

  const fetchTweets = async () => {
    try {
      const tweets = await fetch(config.STOCK_BACKEND + "/tweets?q=" + search);
      const data = await tweets.json();
      const transformedTweets = data.map((tweet) => {
        return {
          id: tweet.id,
        };
      });
      setTweetId(transformedTweets);
    } catch (err) {
      alert("An error occured while fetching user data");
    }
    setLoad(false);
  };

  useEffect(() => {
    // if (loading) return;
    if (!search) return;
    // if (!user) return navigate("/");

    fetchTweets();
  }, [load, search]);

  const RecentTweets = tweetId.map((data) => {
    return (
      <TwitterTweetEmbed
        onLoad={function noRefCheck() {}}
        tweetId={data.id}
        key={data.id}
      />
    );
  });

  return (
    <div className="tweets__wrapper">
      <TextField id="outlined-basic" placeholder="Search" variant="outlined" className="tweet__search"  onChange={(e) => setSearch(e.target.value)}/>  
      <div className="tweets__scroll">
        {RecentTweets}
      </div>
    </div>
  );
}

export default Tweets;
