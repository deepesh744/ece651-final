import React from 'react'
import {makeStyles, Container, Typography} from "@material-ui/core"
import { Button } from '@mui/material';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles(() => ({
  
  stockStyle: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",

  },
  cryptoStyle:{
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",
      paddingTop:30,
  },
  tagline: {
      display:"flex",
      height: "40%",
      flexDirection:"column",
      justifyContent: "center",
      textAlign: "center",
  },

}));

function MenuOptions() {

  const classes = useStyles();
  const history = useHistory();

  return (
    <div>
    <div className={classes.stockStyle}>
      <Button
        variant="contained"
        size="large"
        style={{ backgroundColor: "#EEBC1D" }}
        onClick={() => history.push(`/stockPlayground`)}
      >
        Explore Stock
      </Button>
    </div>
    <div className={classes.cryptoStyle}>
      <Button
        variant="contained"
        size="large"
        style={{ backgroundColor: "#EEBC1D" }}
        onClick={() => history.push(`/crypto`)}
      >
        Explore Crypto
      </Button>
    </div>
    </div>
  )
}

export default MenuOptions