import React from 'react';
import { render, screen, fireEvent } from "@testing-library/react";
import ContextAPI from '../../ContextAPI'
import MenuOptions from './MenuOptions';

describe("Menu options component", () => {
    it('should render without crashing', () => {
        const div = document.createElement('div');
        render(<MenuOptions />, div);
      });
      
    it('should render message', () => {
        render(
        <ContextAPI>
            <MenuOptions />
        </ContextAPI>
        );
        expect(screen.getByText('Explore Stock')).toBeInTheDocument();
    });
})
