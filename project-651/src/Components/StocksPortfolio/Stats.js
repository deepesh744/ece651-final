import React, { useState, useEffect, useContext } from "react";
import "./Stats.css";
import axios from "axios";
import StatsRow from "./StatsRow";
import { db } from "../../firebase/firebase";
import StatsRowPortfolio from "./StatsRowPortfolio";
import _ from "lodash";
import { CryptoState } from "../../ContextAPI";

const BASE_URL = "https://finnhub.io/api/v1/quote?symbol=";
const key = "c7l5u0aad3i9ji44gv40";
const KEY_URL = `&token=${key}`;
const stocksList = [
  "AAPL",
  "ACB",
  "ADBE",
  "AMD",
  "AMZN",
  "BA",
  "BABA",
  "CGC",
  "CMG",
  "CRM",
  "CRON",
  "DIS",
  "FB",
  "GOOGL",
  "HD",
  "HEXO",
  "HLT",
  "IIPR",
  "JPM",
  "LHX",
  "LMT",
  "MA",
  "MCD",
  "MSFT",
  "MU",
  "NFLX",
  "NOC",
  "NOW",
  "NVDA",
  "PYPL",
  "QSR",
  "ROKU",
  "SBUX",
  "SHOP",
  "V",
  "WMT",
];

function Stats() {
  const [stockData, setstockData] = useState([]);
  const [clickedStock, setClickedStock] = useState("AAPL");
  const [myStocks, setMyStocks] = useState([]);
  const { setMyStockList } = CryptoState();

  const getMyStocks = () => {
    db.collection("myStocks").onSnapshot((snapshot) => {
      let promises = [];
      let tempData = [];
      snapshot.docs.map((doc) => {
        promises.push(
          getStocksData(doc.data().ticker).then((res) => {
            tempData.push({
              id: doc.id,
              data: doc.data(),
              info: res.data,
            });
          })
        );
      });
      Promise.all(promises).then(() => {
        console.log("my stocks", tempData);
        if (tempData.length) {
          const myPurchasedStockMap = {};
          tempData.forEach((stock) => {
            const { shares, ticker } = stock.data;
            console.log(stock, stock.data);
            if (shares) {
              myPurchasedStockMap[ticker] = shares;
            }
          });
          console.log(myPurchasedStockMap);
          setMyStockList(myPurchasedStockMap);
        }
        setMyStocks(tempData);
      });
    });
  };

  //Getting the stocks data from Finnhub
  const getStocksData = async (stock) => {
    console.log("Stock Name,", stock);
    return axios.get(`${BASE_URL}${stock}${KEY_URL}`);
  };

  useEffect(() => {
    getMyStocks();
    let tempStocksData = [];
    let promises = [];
    stocksList.map((stock) => {
      promises.push(
        getStocksData(stock).then((res) => {
          tempStocksData.push({
            name: stock,
            ...res.data,
          });
        })
      );
    });

    Promise.all(promises).then(() => {
      setstockData(_.sortBy(tempStocksData, "name"));
    });
  }, []);

  const renderMyStocks = () => {
    if (myStocks.length) {
      return myStocks.map((stock) => (
        <StatsRowPortfolio
          key={stock.data.ticker}
          name={stock.data.ticker}
          openPrice={stock.info.o}
          shares={stock.data.shares}
          price={stock.info.c}
        />
      ));
    }
    return <div>No Stocks in Portfolio!</div>;
  };

  return (
    <div className="stats">
      <div className="stats__container">
        

        <div className="stats__header stats__lists">
          <p>Lists</p>
        </div>
        <div className="stats__container">
          <div className="stats__rows">
            {/* For stocks we can buy */}
            {stockData.map((stock) => (
              <StatsRow
                key={stock.name}
                name={stock.name}
                openPrice={stock.o}
                price={stock.c}
                clickedStock={clickedStock}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Stats;
