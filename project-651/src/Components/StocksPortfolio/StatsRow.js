import React, { useContext, useState } from "react";
import "./StatsRow.css";
import StockChart_increase from "../../images/stock.svg";
import StockChart_decrease from "../../images/stock2.svg";
import numeral from "numeral";
import { db } from "../../firebase/firebase";
import { CryptoState } from "../../ContextAPI";
import {doc} from '@firebase/firestore'
import { setDoc } from "firebase/firestore";

function StatsRow(props) {
  const percentage = ((props.price - props.openPrice) / props.openPrice) * 100;
  const { selectedStock, setSelectedStock, myStockList, balance, setBalance, user, watchlist,setAlert } = CryptoState();

  const isWatchlist = watchlist.includes(props?.name);

  const addToWatchlist = async () => {
    const databaseRef = doc(db,"watchlist",user.uid);

    try{
      console.log("Props.name:", props.name);
      await setDoc(databaseRef, 
        {stocks:watchlist?[...watchlist,props.name]:[props?.name],
      });
      setAlert({
        open: true,
        message: `${props.name} Added to the WatchList!`,
        type: "success"
      });
    } catch(error) {
      setAlert({
        open: true,
        message: error.message,
        type: "error"
      });
    }
  }

  const removeFromWatchlist = async () => {

    const databaseRef = doc(db,"watchlist",user.uid);

    try{
      console.log("Props.name:", props.name);
      await setDoc(databaseRef, 
        {stocks: watchlist.filter((watch) => watch !== props?.name)},
        {merge:"true"}
      );
      setAlert({
        open: true,
        message: `${props.name} Removed from the WatchList!`,
        type: "success"
      });
    } catch(error) {
      setAlert({
        open: true,
        message: error.message,
        type: "error"
      });
    }
  }

  var StockChart = null;
  if (percentage > 0) {
    StockChart = StockChart_increase;
  } else {
    StockChart = StockChart_decrease;
  }

  var sign = "";
  if (percentage > 0) {
    sign = "+";
  }

  var row__percentage = "";
  if (sign === "+") {
    row__percentage = "positive";
  } else {
    row__percentage = "negative";
  }

  var price = 0;
  if (props.shares !== undefined) {
    price = props.price * props.shares;
  } else {
    price = props.price;
  }

  return (
    <div
      className="row"
      style={
        selectedStock === props.name
          ? { background: "#302f2f" }
          : { background: "#1E2023" }
      }
    >
      <div className="row__container">
        <div className="btn-group">
          {user && (<button
            variant = "outlined"
            style={{
              width: "100%",
              height: "flex",
              backgroundColor: isWatchlist ? "#ff0000":"#EEBC1D",
            }}
            onClick={isWatchlist? removeFromWatchlist : addToWatchlist}
          >
            {isWatchlist? "Remove from Watchlist": "Add to Wishlist"}
          </button>)}
        </div>

        <div
          className="stock-details"
          onClick={() => setSelectedStock(props?.name)}
        >
          <div className="row__intro">
            <h1>{props?.name}</h1>
            <p>{props.shares && props.shares + " shares"}</p>
          </div>
          <div className="row__chart">
            <img src={StockChart} height={16} />
          </div>
          <div className="row__numbers">
            <p className="row__price">${numeral(price).format("0,0.00")}</p>
            <p className={row__percentage}>
              {" "}
              {sign}
              {Number(percentage).toFixed(2)}%
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default StatsRow;
