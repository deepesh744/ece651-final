import React from 'react'
import { Link, Router } from 'react-router-dom'
import './TutorialPage.css'
import { Container, Grid, Card, CardContent, CardActions, CardMedia, Typography, Button, Divider } from '@mui/material';

function TutorialPage() {
  return (
    <>
        <div className="container">
            <section className="intro">
                <div className="banner__container">
                    <h1>Stock Market and Financial Education</h1>
                    <p>An in-depth collection of stock market and financial education accessible to all</p>
                </div>
            </section>
        <Container sx={{ py: 8 }} maxWidth="md">
          {/* End hero unit */}
          <Grid container spacing={4}>
            {/* {cards.map((card) => ( */}
              <Grid item key="1" xs={12} sm={6} md={4}>
                <Card
                  sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
                >
                  <CardContent sx={{ flexGrow: 1 }}>
                    <Typography gutterBottom variant="h5" component="h2" className='card__title'>
                      Introduction to Stock Markets
                    </Typography>
                    <Divider style={{borderWidth:"3px", borderColor:"red", marginBottom:"10px"}}/>
                    <Typography>
                    Investing ensures financial security, and the Stock market plays a pivotal role in this domain, 
                    it is a place where people buy/sell shares of publicly listed companies.
                    In this module, you will learn about the fundamentals of the stock market, how to get started,
                    how it functions and the various intermediaries that appertain it.
                    </Typography>
                    <Divider style={{borderWidth:"3px", borderColor:"red", marginTop:"10px"}}/>
                  </CardContent>
                  <CardActions>
                    <Button size="small" component={Link} to="/introduction-to-stock-markets">View</Button>
                  </CardActions>
                </Card>
              </Grid>
              <Grid item key="2" xs={12} sm={6} md={4}>
                <Card
                  sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
                >
                  <CardContent sx={{ flexGrow: 1 }}>
                    <Typography gutterBottom variant="h5" component="h2" className='card__title'>
                      Technical Analysis
                    </Typography>
                    <Divider style={{borderWidth:"3px", borderColor:"green", marginBottom:"10px"}}/>
                    <Typography>
                    Technical Analysis (TA) plays an important role in developing a point of view. Like every other research, 
                    TA also has its own attributes. In this module, we will discover all those complex attributes of TA, 
                    study various patterns, indicators and theories that will help you as a trader to find upright trading 
                    opportunities in the market.
                    </Typography>
                    <Divider style={{borderWidth:"3px", borderColor:"green", marginTop:"10px"}}/>
                  </CardContent>
                  <CardActions>
                  <Button size="small" component={Link} to="/technical-analysis">View</Button>
                  </CardActions>
                </Card>
              </Grid>
              <Grid item key="3" xs={12} sm={6} md={4}>
                <Card
                  sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
                >
                  <CardContent sx={{ flexGrow: 1 }}>
                    <Typography gutterBottom variant="h5" component="h2" className='card__title'>
                      Fundamental Analysis
                    </Typography>
                    <Divider style={{borderWidth:"3px", borderColor:"blue", marginBottom:"10px"}}/>
                    <Typography>
                    Fundamental Analysis (FA) is a holistic approach to study a business. This module will help you 
                    understand Equity research, read financial statements, annual reports, calculation of Financial Ratio,
                    Analysis and most importantly evaluate the intrinsic value of a stock to find long-term investing 
                    opportunities.
                    </Typography>
                    <Divider style={{borderWidth:"3px", borderColor:"blue", marginTop:"10px"}}/>
                  </CardContent>
                  <CardActions>
                    <Button size="small" component={Link} to="/fundamental-analysis">View</Button>
                  </CardActions>
                </Card>
              </Grid>
            {/* ))} */}
          </Grid>
          
        </Container>
        </div>
    </>
  ); 
}

export default TutorialPage