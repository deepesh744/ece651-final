import React from 'react'
import { CryptoState } from '../../ContextAPI';
import {
  Container,
  createTheme,
  ThemeProvider,
  Typography,
  makeStyles,
} from "@material-ui/core";
import { numberWithCommas } from "../CryptoInfo/CoinsTable";
import { AiFillDelete } from "react-icons/ai";
import { doc, setDoc } from "firebase/firestore";
import { db } from "../../firebase/firebase";


const useStyles = makeStyles({
  coin: {
    padding: 10,
    borderRadius: 5,
    color: "black",
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#EEBC1D",
    boxShadow: "0 0 3px black",
  },
});


function WatchlistPage() {

    const {watchlist, setAlert, coins, user} = CryptoState();

    const symbol = "$";

    const classes = useStyles();

    const darkTheme = createTheme({
      palette: {
        primary: {
          main: "#fff",
        },
        type: "dark",
      },
    });

    const removeFromWatchlist = async (coin) => {
      const coinRef = doc(db, "watchlist", user.uid);
      try {
        await setDoc(
          coinRef,
          { coins: watchlist.filter((wish) => wish !== coin?.id) },
          { merge: true }
        );
  
        setAlert({
          open: true,
          message: `${coin.name} Removed from the Watchlist !`,
          type: "success",
        });
      } catch (error) {
        setAlert({
          open: true,
          message: error.message,
          type: "error",
        });
      }
    };

  return (
    <ThemeProvider theme={darkTheme}>
    <Container style={{ textAlign: "center" }}>
        <Typography
          variant="h5"
          style={{ margin: 18, fontFamily: "Montserrat" }}
        >
          Stocks in Watchlist
        </Typography>
        <div>
          {coins.map((coin) => {
            const profit = coin.price_change_percentage_24h > 0;
            if(watchlist.includes(coin.id))
              return (
                <div className={classes.coin}>
                  <span>{coin.name}</span>
                  <span 
                  style={{
                    color: profit > 0 ? "rgb(14, 203, 129)" : "red",
                    fontWeight: 500,
                    display: "flex"
                  }}>
                    {profit && "+"}
                    {coin.price_change_percentage_24h.toFixed(2)}%
                  </span>
                  <span style={{ display: "flex", gap: 8 }}>
                    {symbol}{" "}
                    {numberWithCommas(coin.current_price.toFixed(2))}
                    <AiFillDelete
                      style={{ cursor: "pointer" }}
                      fontSize="16"
                      onClick={() => removeFromWatchlist(coin)}
                    />
                  </span>
                </div>
              )
          })}
        </div>
    </Container>
    </ThemeProvider>

  )
}

export default WatchlistPage