import React from 'react'
import './IntroStockPage.css'
function IntroStockPage() {
  return (
    <>
        <div className="container">
            <section className="intro">
                <div className="banner__container">
                    <h1 className='title'>The Need to Invest</h1>
                    <h3 className='sub__title'>1.1 Where to Invest</h3>
                    <p className='content'> When it comes to investing, one has to choose an asset class that suits the individuals 
                        risk and return temperament. An asset class is a category of investment with particular 
                        risk and return characteristics. The following are some of the popular asset classes.
                    </p>
                    <ol style={{margin:"20px"}}>
                            <li>Fixed income instruments </li>
                            <li>Equity</li>
                            <li>Real estate</li>
                            <li>Commodities (precious metals)</li>
                    </ol>
                    <h3 className='sub__title'>1.2 What are the things to know before investing</h3>
                    <p className='content'> Investing is a great option, but before you venture into investments, it is good to be aware of the following…
                    </p>
                    <ol style={{margin:"20px"}}>
                            <li>Risk and Return go hand in hand. Higher the risk, higher the return. Lower the risk; lower is the return.</li>
                            <li>Investment in fixed income is a good option if you want to protect your principal amount. 
                                It is relatively less risky. However, you have the risk of losing money when you adjust the 
                                inflation return. Example - A fixed deposit which gives you 9% when the inflation is 10% means you are losing a net 1% per annum. Fixed-income investment is best suited for ultra risk-averse investors.</li>
                            <li>Investment in Equities is a great option. It is known to beat inflation over a long period of 
                                time. Historically equity investment has generated returns close to 14-15%. However, equity 
                                investments can be risky.</li>
                            <li>Real Estate investment requires a large outlay of cash and cannot be done with smaller amounts. Liquidity is 
                                another issue with real estate investment - you cannot buy or sell whenever you want. You always have to wait 
                                for the right time and the right buyer or seller to transact with you.</li>
                            <li>Gold and silver are relatively safer, but the historical return on such investment has not been very encouraging.</li>
                    </ol>
                    <h1 className='title'>Regulators</h1>
                    <h3 className='sub__title'>2.1 What is the stock market?</h3>
                    <p className='content'> Investing in equities is an important investment that we make to generate inflation-beating returns. This was the conclusion we drew from the previous chapter. Having said that, how do we go about investing in equities? Clearly, before we dwell further into this topic, it is essential to understand the ecosystem in which equities operate.  
                    </p>
                    <p className='content'> Just like the way we go to the neighbourhood Kirana store or a supermarket to shop for our daily needs, similarly, we go to the stock market to shop (read as transact) for equity investments. The stock market is where everyone who wants to transact in shares goes to. Transact, in simple terms, means buying and selling. You can't buy/sell shares of a public company like Infosys without transacting through the stock markets for all practical purposes.

The main purpose of the stock market is to help you facilitate your transactions. So if you are a buyer of a share, the stock market helps you meet the seller and vice versa.

Now unlike a supermarket, the stock market does not exist in a brick and mortar form. It exists in electronic form. You access the market electronically from your computer and go about conducting your transactions (buying and selling of shares). 
                    </p>
                    <p>It is also important to note that you can access the stock market via a registered intermediary called the stockbroker. We will discuss more the stockbrokers at a later point.

There are two main stock exchanges in India that make up the stock markets. They are the Bombay Stock Exchange (BSE) and the National Stock Exchange (NSE). Besides these two exchanges, there are many other regional stock exchanges like Bangalore Stock Exchange, Madras Stock Exchange that are more or less getting phased out and don't really play any meaningful role anymore.</p>
                    <h3 className='sub__title'>2.2 The Regulator</h3>
                    <p className='content'> The objective of the Regulator is to promote the development of stock exchanges, protect the interest of retail investors, and regulate market participants and financial intermediaries' activities. In general, they ensures:
                    </p>
                    <ol style={{margin:"20px"}}>
                            <li>Stockbrokers and sub-brokers conduct their business fairly </li>
                            <li>Participants don’t get involved in unfair practices</li>
                            <li>Small retail investors interests are protected</li>
                            <li>Large investors with huge cash pile should not manipulate the markets</li>
                    </ol>
                    <h1 className='title'>Financial Intermediaries</h1>
                    <h3 className='sub__title'>3.1 Overview</h3>
                    <p className='content'> From the time you access the market - let's say, to buy a stock till the stocks come and hit your DEMAT account, many corporate entities are actively involved in making this work for you. These entities play their role quietly behind the scene, always complying with the rules laid out by SEBI and ensure an effortless and smooth experience for your transactions in the stock market. These entities are generally referred to as the Financial Intermediaries.

Together, these financial intermediaries, interdependent of one another, create an ecosystem in which the financial markets exist. This chapter will help you get an overview of what these financial intermediaries are and the services they offer.
                    </p>
                    <h3 className='sub__title'>3.2 The Stock Broker</h3>
                    <p className='content'> The stockbroker is probably one of the most important financial intermediaries that you need to know. A stockbroker is a corporate entity, registered as a trading member with the stock exchange and holds a stockbroking license. They operate under the guidelines prescribed by SEBI.

A stockbroker is your gateway to stock exchanges. First, you need to open something called a ‘Trading Account’ with a broker who meets your requirements. Your requirement could be as simple as the proximity between the broker’s office and your house. Simultaneously, it can be as complicated as identifying a broker who can provide you with a single platform using which you can transact across multiple exchanges across the world. At a later point, we will discuss what these requirements could be and how to choose the right broker.
                    </p>
                    <p className='content'> A trading account lets you carry financial transactions in the market. A trading account is an account with the broker, which lets the investor buy/sell securities.

So assuming you have a trading account - whenever you want to transact in the markets, you need to interact with your broker. There are a few standard ways through which you can interact with your broker.
                    </p>
                    <h3 className='sub__title'>3.3 Depository and Depository Participants</h3>
                    <p className='content'> When you buy a property, the only way to identify and claim that you actually own the property is by producing the property papers. Hence it becomes essential to store the property papers in a safe and secure place.

Likewise, when you buy a share (a share represents part ownership in a company) the only way to claim your ownership is by producing your share certificate. A share certificate is nothing but a piece of document entitling you as the owner of the shares in a company.
                    </p>
                    <p className='content'> The share certificate in DEMAT format has to be stored digitally. The storage place for the digital share certificate is the ‘DEMAT Account’. A Depository is a financial intermediary which offers the service of the Demat account. A DEMAT account in your name will have all the shares in the electronic format you bought. Think of the DEMAT account as a digital vault for your shares.

As you may have guessed, your broker’s trading account and the DEMAT account from the Depository are interlinked.

For example, if your idea is to buy Infosys shares, then all you need to do is open your trading account, look for Infosys’ prices, and buy it. Once the transaction is complete, the role of your trading account is done. After you buy, the shares of Infosys will automatically come and sit in your DEMAT account.
                    </p>
                    <h3 className='sub__title'>3.4 Banks</h3>
                    <p className='content'> Banks play a very straightforward role in the market ecosystem. They help in facilitating the fund transfer from your bank account to your trading account. You cannot transfer money from a bank account that is not in your name.

You can link multiple bank accounts to your trading through which you can transfer funds and trade. At Bear&Bull, you can add 1 primary bank account and up to 2 secondary bank accounts. You can use all the bank accounts to add funds, but withdrawals are only processed to the primary bank account. Also, dividend payments, money from buybacks will be sent to the primary bank account. The primary bank account is connected to your trading account and with the Depository and the Registrar and transfer agents (RTA).
                    </p>
                </div>
            </section>
        </div>
    </>
  ); 
}

export default IntroStockPage