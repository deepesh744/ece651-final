import React from 'react'
import Newsfeed from '../StockGraphColumn/Newsfeed'
import Stats from '../StocksPortfolio/Stats'
import {CryptoState} from "../../ContextAPI"
import './StockPlaygroundPage.css'
import Tweets from '../Tweets/Tweets'
function StockPlaygroundPage() {

    const {selectedStock} = CryptoState();

  return (

    <div className="App">
        <div className="app__header">
        </div>
    <div className="app__body">
          <div className="app__container">
            <Tweets />
            <Newsfeed stock={selectedStock}/>
            <Stats />
        </div>
    </div>
    </div>

  )
}

export default StockPlaygroundPage
