import React from 'react'
import { Link } from 'react-router-dom'
import './TechAnalysisPage.css'
import { Container, Grid, Card, CardContent, CardActions, CardMedia, Typography, Button, Divider } from '@mui/material';

function TechAnalysisPage() {
  return (
    <>
        <div className="container">
        <section className="intro">
                <div className="banner__container">
                    <h1 className='title'>Introducing Technical Analysis</h1>
                    <h3 className='sub__title'>1.1 Application on asset types</h3>
                    <p className='content'> One of the greatest versatile features of technical analysis is the fact you can apply TA on any asset class as long as the asset type has historical time series data. Time series data in technical analysis context is the price variables’ information, namely – open high, low, close, volume, etc.
<br/>
Here is an analogy that may help. Think about learning how to drive a car. Once you learn how to drive a car, you can literally drive any car. Likewise, you only need to learn technical analysis once. Once you do so, you can apply TA’s concept on any asset class – equities, commodities, foreign exchange, fixed income, etc.
                    </p>
                    <p className='content'> This is probably one of the biggest advantages of TA compared to the other fields of study. For example, one has to study the profit and loss, balance sheet, and cash flow statements when it comes to fundamental analysis of equity. However, fundamental analysis of commodities is completely different.

If you are dealing with an agricultural commodity like Coffee or Pepper, then the fundamental analysis includes analyzing rainfall, harvest, demand, supply, inventory etc. However, the fundamentals of metal commodities are different, so it is for energy commodities. So every time you choose a commodity, the fundamentals change.<br/>
Anyhow, the concept of technical analysis will remain the same irrespective of the asset you are studying. For example, an indicator such as ‘Moving average convergence divergence’ (MACD) or ‘Relative strength index’ (RSI) is used the same way on equity, commodity or currency.
                    </p>
                    <h3 className='sub__title'>1.2 Assumption in Technical Analysis</h3>
                    <p className='content'> Unlike fundamental analysts, technical analysts don’t care whether a stock is undervalued or overvalued. In fact, the only thing that matters is the stocks past trading data (price and volume) and what information this data can provide about the future movement in the security.

Technical Analysis is based on a few key assumptions. One needs to be aware of these assumptions to ensure the best results.
                    </p>
                    <ol style={{margin:"20px"}}>
                            <li>Markets discount everything – This assumption tells us that, all known and unknown information in the public domain is reflected in the latest stock price. For example, there could be an insider buying the company’s stock in large quantity in anticipation of a good quarterly earnings announcement. While he does this secretively, the price reacts to his actions, revealing to the technical analyst that this could be a good buy.</li>
                            <li>IThe ‘how’ is more important than ‘why’ – This is an extension to the first assumption. Going with the same example as discussed above – the technical analyst would not be interested in questioning why the insider bought the stock as long as he knows how the price reacted to the insider’s action.</li>
                            <li>Price moves in trend –  All major moves in the market is an outcome of a trend. The concept of trend is the foundation of technical analysis. For example, the recent upward movement in the NIFTY Index to 7700 from 6400 did not happen overnight. This move happened in a phased manner, in over 11 months. Another way to look at it is that once the trend is established, the price moves in the trend direction.</li>
                            <li>History tends to repeat itself – In the technical analysis context, the price trend tends to repeat itself. This happens because the market participants consistently react to price movements remarkably similar way, every time the price moves in a certain direction. For example, in up trending markets, market participants get greedy and want to buy irrespective of the high price. Likewise, in a downtrend, market participants want to sell irrespective of the low and unattractive prices. This human reaction ensures that the price history repeats itself.</li>
                    </ol>
                    <h1 className='title'>The Support and Resistance</h1>
                    <h3 className='sub__title'>2.1 The Resistance</h3>
                    <p className='content'> As the name suggests, resistance is something which stops the price from rising further. The resistance level is a price point on the chart where traders expect maximum supply (in terms of selling) for the stock/index. The resistance level is always above the current market price.
<br/>
The likelihood of the price rising to the resistance level, consolidating, absorbing all the supply, and declining is high. The resistance is one of the critical technical analysis tools which market participants look at in a rising market. The resistance often acts as a trigger to sell.  
                    </p>
                    <h3 className='sub__title'>2.2 The Support</h3>
                    <p className='content'> Having learnt about resistance, understanding the support level should be quite simple and intuitive. As the name suggests, support is something that prevents the price from falling further. The support level is a price point on the chart where the trader expects maximum demand (in terms of buying) coming into the stock/index. Whenever the price falls to the support line, it is likely to bounce back. The support level is always below the current market price.
<br/>
There is a maximum likelihood that the price could fall until the support, consolidate, absorb all the demand, and then start moving upwards. The support is one of the critical technical level market participants look for in a falling market. The support often acts as a trigger to buy.
                    </p>
                    <h3 className='sub__title'>2.3 Reliability of S&R</h3>
                    <p className='content'> The support and resistance lines are only indicative of a possible reversal of prices. They by no means should be taken for ascertain. Like anything else in technical analysis, one should weigh the possibility of an event occurring (based on patterns) in terms of probability.
<br/>
Current Market Price = 204
<br/>
Resistance = 214
<br/>
The expectation here is that if Ambuja cement starts to move up at all, it is likely to face resistance at 214. Meaning, at 214 sellers could emerge who can potentially drag the prices lower. What is the guarantee that the sellers would come in at 214? In other words, what is the dependence of the resistance line? Honestly, your guess is as good as mine.
<br/>
However, historically it can be seen that whenever Ambuja reached 214, it reacted in a peculiar way leading to the formation of a price action zone. The comforting factor here is that the price action zone is well spaced in time. This mean 214 stands as a time tested price action zone. Therefore keeping the very first rule of technical analysis in perspective, i.e. “History tends to repeat itself” we go with the belief that support and resistance levels will be reasonably honoured.
<br/>
Purely from my personal trading experience, well constructed S&R points are usually well respected.
                    </p>
                    <h1 className='title'>Volumes</h1>
                    <p className='content'>Volume plays a very integral role in technical analysis as it helps us to confirm trends and patterns. Consider volumes as a means to gain insights into how other participants perceive the market.<br/>
                    Volumes indicate how many shares are bought and sold over a given period of time. The more active the share, the higher would be its volume. For example, you decide to buy 100 shares of Amara Raja Batteries at 485, and I decide to sell 100 shares of Amara Raja Batteries at 485. There are a price and quantity match, which results in a trade. You and I together have created a volume of 100 shares. Many people tend to assume volume count as 200 (100 buys + 100 sells), which is not the right way to look at volumes.
                    </p>
                    <h3 className='sub__title'>3.1 The thought process behind the volume trend table</h3>
                    <p className='content'> When institutional investors buy or sell, they obviously do not transact in small chunks. For example, think about India’s LIC; they are one of India’s biggest domestic institutional investors. If they would buy shares of Cummins India, would you think they would buy 500 shares? Obviously not, they would probably buy 500,000 shares or even more. If they were to buy 500,000 shares from the open market, it would start reflecting in volumes. Besides, because they are buying a large chunk of shares, the share price also tends to go up. Usually, institutional money is referred to as “smart money”. It is perceived that ‘smart money’ always makes wiser moves in the market than retail traders. Hence following the smart money seems like a wise idea.
<br/>
If both the price and the volume are increasing this only means one thing – a big player is showing interest in the stock. Going by the assumption that smart money always makes smart choices, the expectation turns bullish, and hence one should look at buying opportunity in the stock.
<br/>
Or as a corollary, whenever you decide to buy, ensure that the volumes are substantial. This means that you are buying along with the smart money.
<br/>
This is exactly what the 1st row in the volume trend table indicates – expectation turns bullish when both the price and volume increases.
                    </p>
                    <h1 className='title'>Moving Averages</h1>
                    <p className='content'>We have all learnt about averages in school, moving average is just an extension of that. Moving averages are trend indicators and are frequently used due to their simplicity and effectiveness. Before we learn moving averages, let us have a quick recap on how averages are calculated.<br/>
                    Assume 5 people are sitting on a nice sunny beach enjoying a nice chilled bottled beverage. The sun is so bright and nice that each one of them ends up drinking several bottles of the beverage.
                    <br/>
                    Assume a 6th person walks in to find out 29 bottles of beverages lying around them. He can quickly get a sense of ‘roughly’ how many bottles each of them consumed by dividing [the total number of bottles]  by [total number of people].
<br/>
In this case, it would be:
=29/5
=5.8 bottles per head.
<br/>
So, the average, in this case, tells us roughly how many bottles each person had consumed. Obviously, there would be few of them who had consumed above and below the average. For example, Person E drank 8 bottles of beverage, which is way above the average of 5.8 bottles. Likewise, person D drank just 3 bottles of beverage, which is way below the average of 5.8 bottles. Therefore the average is just an estimate, and one cannot expect it to be accurate.
                    </p>
                    <h3 className='sub__title'>4.1 A simple application of moving average</h3>
                    <p className='content'>The moving average can be used to identify buying and selling opportunities with its own merit. When the stock price trades above its average price, it means the traders are willing to buy the stock at a price higher than its average price. This means the traders are optimistic about the stock price going higher. Therefore one should look at buying opportunities.<br/>
                    Likewise, when the stock price trades below its average price, it means the traders are willing to sell the stock at a price lesser than its average price. This means the traders are pessimistic about the stock price movement. Therefore one should look at selling opportunities.
<br/>
We can develop a simple trading system based on these conclusions. A trading system can be defined as a set of rules that help you identify entry and exit points.
                    <br/>
                    </p>
                    <h3 className='sub__title'>4.2 Moving average crossover system</h3>
                    <p className='content'>As its evident now the problem with the plain vanilla moving average system is that it generates far too many trading signals in a sideways market. A moving average crossover system is an improvisation over the plain vanilla moving average system. It helps the trader to take fewer trades in a sideways market.
<br/>
Instead of the usual single moving average in a MA crossover system, the trader combines two moving averages. This is usually referred to as ‘smoothing’.
<br/>
A typical example of this would be to combine a 50 day EMA, with a 100 day EMA. The shorter moving average (50 days in this case) is also referred to as the faster-moving average. The longer moving average (100 days moving average) is referred to as the slower moving average.
<br/>
The shorter moving average takes a lesser number of data points to calculate the average, and hence it tends to stick closer to the current market price and therefore reacts more quickly. A longer moving average takes more data points to calculate the average, and hence it tends to stay away from the current market price. Hence the reactions are slower.
                    </p>
                </div>
            </section>
        </div>
    </>
  ); 
}

export default TechAnalysisPage