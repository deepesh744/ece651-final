import { render, screen, fireEvent } from "@testing-library/react";
import TechAnalysis from "./TechAnalysisPage";
describe("login component", () => {
  it("login content", () => {
    render(
        <TechAnalysis />
    );
    expect(screen.getByText(/versatile/i)).toBeInTheDocument();
  });
});