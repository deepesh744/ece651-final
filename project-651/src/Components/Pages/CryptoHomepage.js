import React from 'react'
import Banner from "../CryptoBanner/Banner";
import CoinsTable from "../CryptoInfo/CoinsTable";

const CryptoHomePage = () => {
  return (
    <div>
    <>
      <Banner />
      <CoinsTable />
    </>
    </div>
  )
}

export default CryptoHomePage