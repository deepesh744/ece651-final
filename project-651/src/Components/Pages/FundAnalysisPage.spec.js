import { render, screen, fireEvent } from "@testing-library/react";
import FundAnalysisPage from "./FundAnalysisPage";

describe("login component", () => {
  it("login content", () => {
    render(
        <FundAnalysisPage />
    );
    
    expect(screen.getByText(/holistic/i)).toBeInTheDocument();
  });
});
