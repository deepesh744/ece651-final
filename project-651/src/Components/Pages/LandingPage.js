import React from 'react'
import Banner from '../HomePageBanner/Banner'
import MenuOptions from "../MenuOptions/MenuOptions"

function LandingPage() {
  return (
    <>
    <Banner />
    <MenuOptions />
    </>
  ); 
}

export default LandingPage