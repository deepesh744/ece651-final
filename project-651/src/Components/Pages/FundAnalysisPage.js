import React from 'react'
import { Link } from 'react-router-dom'
import './FundAnalysisPage.css'
import { Container, Grid, Card, CardContent, CardActions, CardMedia, Typography, Button, Divider } from '@mui/material';

function FundAnalysisPage() {
  return (
    <>
        <div className="container">
        <section className="intro">
                <div className="banner__container">
                    <h1 className='title'>Introduction to Fundamental Analysis</h1>
                    <h3 className='sub__title'>1.1 Overview</h3>
                    <p className='content'> Fundamental Analysis (FA) is a holistic approach to study a business. When an investor wishes to invest in a business for the long term (say 3 – 5 years), it becomes essential to understand the business from various perspectives. It is critical for an investor to separate the daily short term noise in the stock prices and concentrate on the underlying business performance. Over the long term, a fundamentally strong company’s stock prices tend to appreciate, thereby creating wealth for its investors.
                    </p>
                    <h3 className='sub__title'>1.2 Tools of FA</h3>
                    <p className='content'> The tools required for fundamental analysis are fundamental, most of which are available for free. Specifically, you would need the following:
                    </p>
                    <ol style={{margin:"20px"}}>
                            <li>The company’s annual report – All the information you need for FA is available in the annual report. You can download the annual report from the company’s website for free</li>
                            <li>Industry-related data – You will need industry data to see how the company under consideration is performing concerning the industry. Basic data is available for free and is usually published in the industry’s association website</li>
                            <li>Access to the news – Daily News helps you stay updated on the latest developments in the industry and the company you are interested in. A good business newspaper or services such as Google Alert can help you stay abreast of the latest news</li>
                            <li>MS Excel – Although not free, MS Excel can be extremely helpful in fundamental calculations</li>
                    </ol>
                    <p className='content'>
                    With just these four tools, one can develop a fundamental analysis that can rival institutional research. You can believe me when I say that you don’t need any other tool to do good fundamental research. In fact, even at the institutional level, the objective is to keep the research simple and logical.
                    </p>
                    <h1 className='title'>Understanding the P&L Statement</h1>
                    <h3 className='sub__title'>2.1 The Profit and Loss statement</h3>
                    <p className='content'> The Profit and Loss statement is also popularly referred to as the P&L statement, Income Statement, Statement of Operations, and Statement of Earnings. The Profit and Loss statement shows what has transpired during a time period.  The P&L statement reports information on:
                    </p>
                    <ol style={{margin:"20px"}}>
                            <li>The revenue of the company for the given period (yearly or quarterly)</li>
                            <li>The expenses incurred to generate the revenues</li>
                            <li>Tax and depreciation</li>
                            <li>The earnings per share number</li>
                    </ol>
                    <h1 className='title'>The Cash Flow statement</h1>
                    <h3 className='sub__title'>3.1 Overview</h3>
                    <p className='content'> Before we understand the cash flow statement, it is important to understand ‘the activities’ of a company. If you think about a company and the various business activities, you will realize that the company’s activities can be classified under one of the three standard baskets. We will understand this in terms of an example.
<br/>
Imagine a business, maybe a very well established fitness centre (Talwalkars, Gold’s Gym etc.) with a sound corporate structure. What are the typical business activities you think a fitness centre would have?
                    </p>
                    <h3 className='sub__title'>3.2 The cash flow statement</h3>
                    <p className='content'> The cash flow statement (CFS), is a financial statement that summarizes the movement of cash and cash equivalents (CCE) that come in and go out of a company. The CFS measures how well a company manages its cash position, meaning how well the company generates cash to pay its debt obligations and fund its operating expenses. As one of the three main financial statements, the CFS complements the balance sheet and the income statement. In this article, we’ll show you how the CFS is structured and how you can use it when analyzing a company.
                    </p>
                    <h3 className='sub__title'>3.3 How the Cash Flow Statement Is Used</h3>
                    <p className='content'> The cash flow statement paints a picture as to how a company’s operations are running, where its money comes from, and how money is being spent. Also known as the statement of cash flows, the CFS helps its creditors determine how much cash is available (referred to as liquidity) for the company to fund its operating expenses and pay down its debts. The CFS is equally as important to investors because it tells them whether a company is on solid financial ground. As such, they can use the statement to make better, more informed decisions about their investments.
                    </p>
                    <p className='content'> Investing activities include any sources and uses of cash from a company’s investments. Purchases or sales of assets, loans made to vendors or received from customers, or any payments related to mergers and acquisitions (M&A) are included in this category. In short, changes in equipment, assets, or investments relate to cash from investing.
<br/>
Changes in cash from investing are usually considered cash-out items because cash is used to buy new equipment, buildings, or short-term assets such as marketable securities. But when a company divests an asset, the transaction is considered cash-in for calculating cash from investing.
                    </p>
                </div>
            </section>
        </div>
    </>
  ); 
}

export default FundAnalysisPage