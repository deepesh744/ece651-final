import React from 'react';
import ReactDOM from 'react-dom';
import Banner from './Banner';
import { render, screen, fireEvent } from "@testing-library/react";
import ContextAPI from '../../ContextAPI'

describe("Banner page component", () => {
    it('should render without crashing', () => {
        const div = document.createElement('div');
        render(<Banner />, div);
      });
      
    it('should render message', () => {
        render(
        <ContextAPI>
            <Banner />
        </ContextAPI>
        );
        expect(screen.getByText('Get all the Info regarding your favorite Crypto Currency')).toBeInTheDocument();
    });
})

