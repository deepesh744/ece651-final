import React, { useState, useEffect } from "react";
import "./Newsfeed.css";
import CandlestickGraph from "../CandleStickGraph/CandlestickGraph";
import axios from "axios";
import moment from "moment";
const BASE_URL = `https://finnhub.io/api/v1/stock/candle?symbol=AAPL&resolution=60&from=${moment()
  .subtract(1, "year")
  .unix()}&to=${moment().unix()}&token=c7l5u0aad3i9ji44gv40`;

function Newsfeed({ stock }) {
  const [stockData, setStockData] = useState(null);

  const getBaseURL = (stockName) => {
    const url = `https://finnhub.io/api/v1/stock/candle?symbol=${stockName}&resolution=60&from=${moment()
      .subtract(1, "year")
      .unix()}&to=${moment().unix()}&token=c7l5u0aad3i9ji44gv40`;
    console.log(url);
    return url;
  };

  useEffect(() => {
    console.log("Selected Stock, ", stock);
    axios
      .get(getBaseURL(stock))
      .then((resp) => {
        console.log("Response", resp);
        setStockData(resp.data);
      })
      .catch((error) => console.log("Error: ", error.message));
  }, [stock]);

  const renderStockData = () => {
    console.log("here", stockData);
    if (stockData) {
      return <CandlestickGraph data={stockData} />;
    }
    return <div className="loader">Loading....</div>;
  };

  return (
    <div className="newsfeed">
      <div className="newsfeed__container">
        <div className="newsfeed__chartSection">
          <div className="newsfeed__portfolio">
            <h1>Candle Chart for {stock}</h1>
            <div className="candle-chart-wrapper">{renderStockData()}</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Newsfeed;