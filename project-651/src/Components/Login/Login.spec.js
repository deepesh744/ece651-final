import { render, screen, fireEvent } from "@testing-library/react";
import Login from "./Login";
import {  getByTestId, waitFor } from "@testing-library/dom";
import ContextAPI from "../../ContextAPI";
import { act } from 'react-dom/test-utils';
describe("login component", () => {
  it("check if login button is enabled", () => {
    const { getByTestId } = render(
      <ContextAPI>
        <Login />
      </ContextAPI>
    );

    const submit = getByTestId("submit");
    expect(submit).not.toHaveClass("Mui-disabled");
  });

  it("login content", () => {
    render(
      <ContextAPI>
        <Login />
      </ContextAPI>
    );
    
    expect(screen.getByText(/Login/i)).toBeInTheDocument();
  });

  it("email input check", () => {
    const utils = render(
      <ContextAPI>
        <Login />
      </ContextAPI>
    );
    const input = utils.getByLabelText("Enter Email");
    fireEvent.change(input, { target: { value: "shrenik@gmail.com" } });

    expect(input.value).toBe("shrenik@gmail.com");
  });
  it("password input check", () => {
    const utils = render(
      <ContextAPI>
        <Login />
      </ContextAPI>
    );
    const input = utils.getByLabelText("Enter Password");
    fireEvent.change(input, { target: { value: "shrenik" } });

    expect(input.value).toBe("shrenik");
  });

//   it("login functionality check", async () => {
//     const utils = render(
//       <ContextAPI>
//         <Login />
//       </ContextAPI>
//     );
//     const email = utils.getByLabelText("Enter Email");
//     fireEvent.change(email, { target: { value: "shrenik@gmail.com" } });
//     const password = utils.getByLabelText("Enter Password");
//     fireEvent.change(password, { target: { value: "shrenik" } });
//     act(() => {
//       fireEvent.click(screen.getByText(/Login/i));
//     });

//     await waitFor(() => {
//         expect(screen.getByText(/Login/i)).toBeInTheDocument();
//     });
//   });
  // test('valid credentials', async () => {
  //     let response = await logInWithEmailAndPassword('shrenik@gmail.com', 'shrenik')
  //     expect(response).toEqual(`Valid Credentials`);
  // });

  // test('invalid credentials', async () => {
  //     let response = await logInWithEmailAndPassword('shrenik@gmail.com', 'shreni')
  //     expect(response).toEqual(`Error: Invalid Email and Password`);
  //   });
});
