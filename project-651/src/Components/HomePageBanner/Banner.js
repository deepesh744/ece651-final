import React from 'react'
import {makeStyles, Container, Typography} from "@material-ui/core"
import Slider from "./Slider"

const useStyles = makeStyles(() => ({
    bannerContent: {
        height:400,
        display: "flex",
        flexDirection: "column",
        paddingTop: 25,
        justifyContent: "space-around",
    },
    tagline: {
        display:"flex",
        height: "40%",
        flexDirection:"column",
        justifyContent: "center",
        textAlign: "center",
    },

}));

function Banner() {
    const classes = useStyles()
  return (
    <div className={classes.banner}>
        <Container className={classes.bannerContent}>

            <div className={classes.tagline}>
                <Typography
                    variant="h3"
                    style={{fontWeight: "bold",marginBottom: 15,fontFamily: "Montserrat",}}>
                    Learn About your Next Investment
                </Typography>

                <Typography
                    variant="subtitle2"
                    style={{color: "darkgrey",textTransform: "capitalize",fontFamily: "Montserrat",}}>
                    Here's some trending stuff out there
                </Typography>
            </div>
            <Slider />
        </Container>
    </div>
  )
}

export default Banner