import React ,{ useState, useEffect} from 'react'
import {makeStyles, Container, Typography} from "@material-ui/core"
import axios from "axios";
import AliceCarousel from "react-alice-carousel";
import {Link} from "react-router-dom"



const HistoricalChart = (id, days = 365) =>
`https://api.coingecko.com/api/v3/coins/${id}/market_chart?vs_currency=usd&days=${days}`;

const TrendingCoins = () =>
`https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=gecko_desc&per_page=10&page=1&sparkline=false&price_change_percentage=24h`;

// const SingleCoin = (id) =>
// `https://api.coingecko.com/api/v3/coins/${id}`;


export function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

const useStyles = makeStyles((theme) => ({
    slider: {
        height:"50%",
        display: "flex",
        alignItems: "center",
    },
    sliderItems: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        cursor: "pointer",
        textTransform: "uppercase",
        color: "white",
    }
}));


function Slider() {

    const [trendingCoin, setTrendingCoin] = useState([]);

    const classes = useStyles();

    const fetchTrendingCoin = async() => {
        const {data} = await axios.get(TrendingCoins());
        setTrendingCoin(data);
    }

    useEffect(() => {
        fetchTrendingCoin();
    }, []);

    const trendingItems = trendingCoin.map((coin) => {
        let profit = coin.price_change_percentage_24h >= 0;

        return(
            <Link className={classes.sliderItems} to={`/coins/${coin.id}`}>
                <img 
                    src={coin?.image}
                    alt={coin.name}
                    height="80"
                    style={{marginBottom: 10}}
                />
                <span>
                    {coin?.symbol}
                    &nbsp;
                    <span style={{color:profit>0 ? "rgb(14,203,129)": "red", fontWeight: 480}}>
                        {profit && "+"} {coin?.price_change_percentage_24h?.toFixed(2)}%
                    </span>
                </span>

                <span style={{ fontSize:22, fontWeight:500}}>
                    $ {numberWithCommas(coin?.current_price.toFixed(2))}
                </span>
            </Link>
        )
    })
    
    const responsive = {
        0: {
            items: 2,
        },
        512: {
            items: 4,
        }
    }

  return (
    <div className={classes.slider}>
        <AliceCarousel
            disableButtonsControls
            maoseTracking
            infinite
            autoPlayInterval={1000}
            animationDuration={1500}
            disableDotsControls
            responsive= {responsive}
            autoPlay
            items = {trendingItems} 
        />
    </div>
  )
}

export default Slider