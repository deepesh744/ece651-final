import { useState } from "react";
import Backdrop from '@mui/material/Backdrop';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Button from '@mui/material/Button';
import {Tab, Tabs, AppBar } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Login from '../Login/Login'
import Register from '../Register/Register'

const useStyles = makeStyles((theme) => ({
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",

    },
    paper: {
      width: 400,
      backgroundColor: "white",
      color: "white",
      borderRadius: 10,
    },
    google: {
      padding: 24,
      paddingTop: 0,
      display: "flex",
      flexDirection: "column",
      textAlign: "center",
      gap: 20,
      fontSize: 20,
    },
  }));

export default function AuthPopup() {

    const classes = useStyles();

    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const [value, setValue] = useState(0);

    //0- Login 1-Register
    console.log(value);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

  return (
    <div>
      <Button
        variant="contained"
        style={{
          width: 85,
          height: 40,
          marginLeft: 15,
          backgroundColor: "#EEBC1D",
        }}
        onClick={handleOpen}
      >
        Login
      </Button>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
        <div className={classes.paper}>

        <AppBar
              position="static"
              style={{
                backgroundColor: "transparent",
                color: "white",
              }}
        >
            <Tabs value={value} onChange={handleChange} variant="fullWidth" style={{borderRadius: 10}}>
            <Tab label="Login"/>
            <Tab label="SignUp"/>
        </Tabs>
        </AppBar>
            {value === 0 && <Login handleClose={handleClose} />}
            {value === 1 && <Register handleClose={handleClose} />}
        </div>
        </Fade>
      </Modal>
    </div>
  );
}