import React from 'react';
import { render, screen, fireEvent } from "@testing-library/react";
import ContextAPI from '../../ContextAPI'
import AuthPopup from './AuthPopup';

describe("Auth popup component", () => {
    it('should render without crashing', () => {
        const div = document.createElement('div');
        render(<AuthPopup />, div);
      });
      
    it('should render message', () => {
        render(
        <ContextAPI>
            <AuthPopup />
        </ContextAPI>
        );
        expect(screen.getByText('Login')).toBeInTheDocument();
    });
})
