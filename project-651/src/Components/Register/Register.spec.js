import { render, screen, fireEvent } from "@testing-library/react";
import Register from "./Register";
import ContextAPI from "../../ContextAPI";

describe("registration component", () => {
  it("check if button is enabled", () => {
    const { getByTestId } = render(
      <ContextAPI>
        <Register />
      </ContextAPI>
    );

    const submit = getByTestId("submit");
    expect(submit).not.toHaveClass("Mui-disabled");
  });

  it("login content", () => {
    render(
      <ContextAPI>
        <Register />
      </ContextAPI>
    );

    expect(screen.getByText(/Sign Up/i)).toBeInTheDocument();
  });
  it("email input check", () => {
    const utils = render(
      <ContextAPI>
        <Register />
      </ContextAPI>
    );
    const input = utils.getByLabelText("Enter Email");
    fireEvent.change(input, { target: { value: "shrenik@gmail.com" } });

    expect(input.value).toBe("shrenik@gmail.com");
  });
  it("password input check", () => {
    const utils = render(
      <ContextAPI>
        <Register />
      </ContextAPI>
    );
    const input = utils.getByLabelText("Enter Password");
    fireEvent.change(input, { target: { value: "shrenik" } });

    expect(input.value).toBe("shrenik");
  });
  it("confirm password input check", () => {
    const utils = render(
      <ContextAPI>
        <Register />
      </ContextAPI>
    );
    const input = utils.getByLabelText("Confirm Password");
    fireEvent.change(input, { target: { value: "shrenik" } });

    expect(input.value).toBe("shrenik");
  });
//   it("sign up functionality check", async () => {
//     const utils = render(
//       <ContextAPI>
//         <Register />
//       </ContextAPI>
//     );
//     act(() => {
//       const email = utils.getByLabelText("Enter Email");
//       fireEvent.change(email, { target: { value: "shren@gmail.com" } });
//       const password = utils.getByLabelText("Enter Password");
//       fireEvent.change(password, { target: { value: "shreniK@" } });
//       const confirmPassword = utils.getByLabelText("Confirm Password");
//       fireEvent.change(confirmPassword, { target: { value: "shreniK@" } });
//       fireEvent.click(screen.getByText(/Sign Up/i));
//     });

//     await waitFor(() => {
//       expect(screen.getByText(/Sign Up/i)).toBeInTheDocument();
//     });
//   });
  // test('email already in use', async () => {
  //     let response = await registerWithEmailAndPassword('Shrenik', 'shrenik@gmail.com', 'shrenik')
  //     expect(response).toEqual(`Error: Email already in use`);
  //   });

  // test('user created', async () => {
  //     let response = await registerWithEmailAndPassword('John Doe', 'johndoewwsa30943@gmail.com', 'johndoE653')
  //     expect(response).toEqual(`New user created`);
  // });
});
