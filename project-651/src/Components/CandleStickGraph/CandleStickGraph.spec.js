import { shallow } from 'enzyme'
import React from 'react'
import CandlestickGraph from './CandlestickGraph'
import { render } from "@testing-library/react";

jest.mock('moment', () => {
    return () => jest.requireActual('moment')('2020-01-01T00:00:00.000Z');
  });

describe("login component", () => {

    const mockData = [{c: [164.4], h:[165.24], l: [163.72], o: [163.72], s: "ok", t:[1646384400], v: [48241] } ];

    it('should render Plotly candlechart', () => {
        const div = document.createElement('div');
        render(<CandlestickGraph data={mockData}/>, div);
        
      });

});