import Plot from "react-plotly.js";
import moment from "moment";
import "./candlestickGraph.css";

const CandlestickGraph = ({ data }) => {
  console.log(data);

  const selectorOptions = {
    buttons: [
      {
        step: "month",
        stepmode: "backward",
        count: 1,
        label: "1m",
      },
      {
        step: "day",
        stepmode: "backward",
        count: 7,
        label: "1w",
      },
      {
        step: "day",
        stepmode: "backward",
        count: 1,
        label: "1d",
      },
      {
        step: "year",
        stepmode: "backward",
        count: 1,
        label: "1y",
      },
      {
        step: "all",
      },
    ],
  };

  const computeTrace = (data) => {
    const { t, c: close, h: high, l: low, o: open } = data;
    const x = t?.map((time) => moment.unix(time).format("YYYY-MM-DD HH:mm:ss"));
    return {
      x,
      close,
      decreasing: { line: { color: "#FF083A" } },
      high,
      increasing: { line: { color: "#28A743" } },
      line: { color: "#E6E6E6" },
      low,
      open,
      type: "candlestick",
      xaxis: "x",
      yaxis: "y",
    };
  };

  return (
    <Plot
      data={[computeTrace(data)]}
      layout={{
        width: 720,
        height: 400,
        margin: {
          t: 50,
          b: 50,
          l: 100,
        },
        xaxis: {
          rangeselector: selectorOptions,
          rangeslider: { visible: false },
          color: "rgb(197, 212, 203)",
          showgrid: false,
        },
        yaxis: {
          color: "rgb(197, 212, 203)",
          showgrid: false,
        },
        plot_bgcolor: "#1E2023",
        paper_bgcolor: "#1E2023",
      }}
      config={{ displayModeBar: false, autosizable: true }}
    />
  );
};

export default CandlestickGraph;
