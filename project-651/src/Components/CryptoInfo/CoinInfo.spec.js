import React from 'react'
import { render } from "@testing-library/react";
import mockedAxios from 'axios';
import CoinInfo from './CoinInfo'

jest.mock("axios");


describe("CoinInfo component", () => {
        it('mocking axios request', async () => {

            //MockData is response from API
            const mockData = { prices: [1648886381452, 46575.64222037866] , market_caps: [1648886381452, 883503418866.9836] , 
                total_volumes:[1648886381452, 29645211156.084686]  };

                //Prop passed through CoinInfo Component
                const mockCoin = { id: 'ethereum', symbol: 'eth', name: 'Ethereum'};

                mockedAxios.get.mockResolvedValueOnce(mockData);
                const div = document.createElement('div');

                const { getByText } = render(<CoinInfo coin={mockCoin}/>, div);

                await wait(() => {
                    expect(getByText(mockData));
                  });

        });

});
