import './App.css';
import {BrowserRouter, Route} from "react-router-dom"
import Header from "./Components/Header/Header"
import Homepage from "./Components/Pages/LandingPage"
import {makeStyles} from "@material-ui/core"
import Alert from "./Alert"
import StockPlaygroundPage from './Components/Pages/StockPlaygroundPage'
import WatchlistPage from './Components/Pages/WatchlistPage';
import CryptoHomePage from './Components/Pages/CryptoHomepage'
import CoinPage from './Components/Pages/CoinPage'

import TutorialPage from './Components/Pages/TutorialPage';
import IntroStockPage from './Components/Pages/IntroStockPage';
import TechAnalysisPage from './Components/Pages/TechAnalysisPage';
import FundAnalysisPage from './Components/Pages/FundAnalysisPage';
function App() {

  //makestyles takes a calback
  const useStyles = makeStyles(() => ({
    App: {
      backgroundColor: '#14161a',
      color: 'white',
      minHeight: "100vh",
    }
  }));

  const classes = useStyles();
  return (
    <div className="App">
      <BrowserRouter>
        <div className={classes.App}>
          <Header /> 
          <Route path='/' component={Homepage} exact/>
          <Route path='/stockPlayground' component={StockPlaygroundPage} exact />
          <Route path='/watchlist' component={WatchlistPage} exact/>
          <Route path="/crypto" component={CryptoHomePage} exact/>
          <Route path="/crypto/coins/:id" component={CoinPage} exact/>
          <Route path='/tutorial' component={TutorialPage} />
          <Route path='/introduction-to-stock-markets' component={IntroStockPage} />
          <Route path='/technical-analysis' component={TechAnalysisPage} />
          <Route path='/fundamental-analysis' component={FundAnalysisPage} />
        </div>
        <Alert />
      </BrowserRouter>
    </div>
  );
}

export default App;
